from setuptools import setup
from codecs import open
from os import path


__version__ = '0.0.1'

SETUP_DIR = path.abspath(path.dirname(__file__))


# Get the long description from the README file. ie: In RestrucTexT
with open(path.join(SETUP_DIR, 'README')) as f:
    long_description = f.read()


# get the dependencies and installs
with open(path.join(SETUP_DIR, 'requirements.txt')) as f:
    all_reqs = f.read().split('\n')


install_requires = [x.strip() for x in all_reqs if 'git+' not in x]
dependency_links = [x.strip().replace('git+', '') for x in all_reqs if x.startswith('git+')]


setup(
    name='MontyAi',
    version=__version__,
    url='https://bitbucket.org/traBpUkciP/monty-in-python-life-of-jarvis',
    packages=['monty_dev'],
    package_data = {'monty_dev': []},
    license='MIT License',
    author='Scott Doucet',
    author_email='duroktar@gmail.com',
    description='Monty in Python - An extensible AI written in Python',
    long_description=long_description,
    # keywords='ai accessibility',
    install_requires=install_requires,
    dependency_links=dependency_links,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Games/Entertainment',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        # 'Programming Language :: Python :: 3',   # TODO: Needs Testing.. print statments should be it...
        # 'Programming Language :: Python :: 3.5',
    ],
    entry_points={
          'console_scripts': [
              'Monty = monty_dev.__main__:main'
          ]
      }
)