from __future__ import print_function
import os, sys

from monty_dev import MainApp, gui

def deploy():
    app = MainApp()
    app.start(debug=False)

def debug():
    app = MainApp()
    app.start(debug=True)

def setup():
    choice = None
    while choice not in ['y', 'n']:
        choice = raw_input("This will install any needed textblob dependancies. Continue? [n] y >>> ") or 'n'
    if choice == 'y': 
        os.system("python -m textblob.download_corpora")
        print("Success!")
    else:
        print('Canceled')

if __name__ == '__main__':

    # Taken from http://stackoverflow.com/questions/7936572/python-call-a-function-from-string-name
    args = sys.argv[1:]
    user_arg = args[0]

    possibles = globals().copy()
    possibles.update(locals())
    function = possibles.get(user_arg)
    if not function:
        raise NotImplementedError("Function %s not implemented" % user_arg)
    function()
