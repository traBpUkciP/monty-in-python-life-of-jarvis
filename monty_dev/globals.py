class Globals(object):
    ON_EXIT = False
    IS_BUSY = False
    ON_HEARD = False
    IS_LISTENING = False
    QUEUE = []

    def get_global(self, name):
        return self.__getattribute__(name)

    def update(self, name, value):
        self.__setattr__(name, value)