import json
import sys
from adapt.entity_tagger import EntityTagger
from adapt.tools.text.tokenizer import EnglishTokenizer
from adapt.tools.text.trie import Trie
from adapt.intent import IntentBuilder
from adapt.parser import Parser
from adapt.engine import DomainIntentDeterminationEngine


# TODO: I don't remember what this is..



class Intentifier(object):
    def __init__(self):
        self.engine = DomainIntentDeterminationEngine()
        self.tokenizer = EnglishTokenizer
        self.trie = Trie()
        self.tagger = EntityTagger(self.trie, self.tokenizer)
        self.parser = Parser(self.tokenizer, self.tagger)

    def register_entities(self, kw_list, kw_id, domain):
        for kw in kw_list:
            self.engine.register_entity(kw, kw_id, domain)

    def register_domain(self, name, tokenizer=None, trie=None):
        self.engine.register_domain(name, tokenizer, trie)

if __name__ == '__main__':
    test = Intentifier()
    test.register_domain('Domain1')
