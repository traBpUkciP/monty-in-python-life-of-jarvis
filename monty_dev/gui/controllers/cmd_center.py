import sys
from PyQt4.QtCore.Qt import FramelessWindowHint, WA_TranslucentBackground
from PyQt4.QtGui import QApplication, QMainWindow
from ..gui.command_center import Ui_MainWindow

app = QApplication(sys.argv)
window = QMainWindow()
window.setWindowFlags(FramelessWindowHint)
window.setAttribute(WA_TranslucentBackground)
ui = Ui_MainWindow()
ui.setupUi(window)

window.show()
sys.exit(app.exec_())

