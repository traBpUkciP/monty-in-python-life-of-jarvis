#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Copyright 2016 Scott Doucet

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
from __future__ import print_function
import wikipedia
import webbrowser
# from fuzzywuzzy import fuzz


class Plugin:

    def __init__(self):
        self.name = "The Wikipedia Search Plugin"

    def main(self, query):
        clean_query = self.cleanup(query)
        response = wikipedia.search(clean_query)
        for i in range(1):
            page = wikipedia.page(response[i])
            webbrowser.open(page.url)

    def cleanup(self, string):
        return string

if __name__ == "__main__":
    # main(123)
    test = Plugin()
    test.main("horses")
    print(test.name)
