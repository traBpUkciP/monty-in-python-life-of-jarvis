#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Copyright 2016 Scott Doucet

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
from __future__ import print_function
import os, sys
from time import sleep
import threading
import logging
from random import choice
from Queue import Queue
import difflib

from eventdispatcher import EventDispatcher

from .plugins import PluginHandler
from .utils.speech import SpeechEngine
from .listener import Listener
from .classifier import ClassifierDB

#########
# Paths ############
PATH = os.path.dirname(os.path.abspath(__file__))
HOME_PATH = os.path.expanduser('~')
MONTY_USER_DIR = os.path.join(HOME_PATH, '.monty')
if not os.path.exists(MONTY_USER_DIR):
    os.mkdir(MONTY_USER_DIR)
####################


#########
# Debug ############
LOGFILE = os.path.join(MONTY_USER_DIR, 'debug.log')
if not os.path.isfile(LOGFILE):
    open(LOGFILE, 'a')
FORMAT = '%(asctime)s - [%(levelname)s] [(%(threadName)-10s)] [%(funcName)s] %(message)s'
logging.basicConfig(filename=LOGFILE,
                    level=logging.DEBUG,
                    format=FORMAT,
                    datefmt='%m/%d/%Y %I:%M:%S %p')
#####################


#########
# TODO: Make use of this.. I like it..
class Options:
    """ A structure that can have any fields defined. """
    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __repr__(self):
        args = ['%s=%s' % (k, repr(v)) for (k, v) in vars(self).items()]
        return 'Options(%s)' % ', '.join(args)
#######################


class MainApp(EventDispatcher):
    """ TODO: Docstrings... merp

    """
    def __init__(self):
        super(MainApp, self).__init__()
        self.mic = Listener()
        self.brain = ClassifierDB(os.path.join(PATH, 'classifier_adv'))
        self.plugins = PluginHandler()
        self.speech = SpeechEngine()
        self.speech.start()
        self.command = Queue()
        # self.start()

    def main_thread(self):
        """ This method is the main thread runner """
        running = [True]

        def run_thread():
            while running[0]:
                self.command.put(self.mic.recognized_speech.get())

        def stopper():
            running[0] = False
            main_thread.join()  # block until the background thread is done
        main_thread = threading.Thread(target=run_thread)
        main_thread.daemon = True
        main_thread.start()
        return stopper

    def run_action(self, name, arg):
        self.plugins.use_script(name, arg)

    def db_editor(self):
        pass

    def start(self, debug=True):
        
        # TODO: Implement debug mode
        if debug:
            print("Debug mode")
        
        #  MAIN_LOOP
        while True:

            #  Check the listener output queue for a speech event and get it
            if not self.mic.recognized_speech.empty():
                cmd = self.mic.recognized_speech.get(block=False)
                self.command.put(cmd)
                # print(cmd)

            #  Need to send event out to all the classifiers
            if not self.command.empty():
                cmd = self.command.get(block=False)
                main_cls = self.brain.classifier_classify(cmd)
                # print(main_cls)
                func, conf, speech = main_cls
                responses = self.brain.database.action_responses(func)
                # print(responses)
                cases = [x.lower() for x, y in self.brain.training_set if y == func]
                # print(cases)
                test = [speech for _ in range(len(cases))]
                # print(test)

                # print(cases)
                d = difflib.Differ()
                diff = d.compare(cases, [speech])

                if conf > 0.5:
                    self.run_action(func, speech)
                    self.speech.speak(choice(responses))
                # else:
                #     self.run_action(func, speech)
            self.speech.update()
            sleep(0.15)

    def exit(self):
        """ Used to exit program

        """
        self.speech.destroy()
        quit()


if __name__ == '__main__':
    App = MainApp()
    App.start()
