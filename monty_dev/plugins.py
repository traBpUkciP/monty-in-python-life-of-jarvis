#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Copyright 2016 Scott Doucet

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
from __future__ import print_function
import os, sys
from glob import glob1


class PluginHandler:
    """
    https://www.reddit.com/r/Python/comments/1qaepq/very_basic_plugin_system/

    """

    def __init__(self):

        self.scripts = {}
        self.path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'scripts')
        self.load_scripts()

    def load_scripts(self):
        scripts = glob1(self.path, '*.py')
        sys.path.insert(0, self.path)
        for i in scripts:
            if i.startswith('__'):
                continue
            fname, ext = i[:-3], i[-3:]
            mod = __import__(fname)
            self.scripts[fname] = mod.Plugin()
        sys.path.pop(0)

    def use_script(self, script, string):
        if script not in self.scripts:
            return
        func = self.scripts[script]
        func.main(string)

    def get_script(self, name):
        return self.scripts[name] if name in self.scripts else None

def test_plugins():
    plugs = PluginHandler()
    plugs.load_scripts()
    plugs.use_script('google_search', "horses")
    x = plugs.get_script('boobs')
    y = plugs.get_script('google_search')
    print(x, y)
    print(y.name)


