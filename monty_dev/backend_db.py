#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Copyright 2016 Scott Doucet

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
import os
import sys
import logging
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QFileDialog, QPalette

import gui.views.db_viewer as design
from .classifier import ClassifierDB, db_adapter, action_adapter

PATH = os.path.dirname(os.path.abspath(__file__))

LOGFILE = os.path.join(PATH, 'logger.log')
FORMAT = '%(asctime)s - [%(levelname)s] [(%(threadName)-10s)] [%(funcName)s] %(message)s'
logging.basicConfig(filename=LOGFILE,
                    level=logging.DEBUG,
                    format=FORMAT,
                    datefmt='%m/%d/%Y %I:%M:%S %p')


class ExampleApp(QtGui.QMainWindow, design.Ui_ActionEditor):
    db_path = None

    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)  # This is defined in db_viewer.py file automatically
        palette = QPalette()
        palette.setBrush(QPalette.Background, QtGui.QBrush(QtGui.QPixmap(os.path.join(PATH, 'images', 'ui_background.jpg'))))
        self.setPalette(palette)
        self.changed = False
        self.current_action = None
        self.classifier = ClassifierDB(os.path.join(PATH, 'classifier_adv'))
        self.database = self.classifier.database
        self.filename = self.classifier.save_file
        self.SaveDb.clicked.connect(self.save_db)
        self.LoadDb.clicked.connect(self.load_db)
        self.ActionList.activated.connect(self.refresh)
        self.AddAction.clicked.connect(self.on_add_action)
        self.AddHint.clicked.connect(self.add_hint)
        self.AddResponse.clicked.connect(self.add_response)
        self.EditCallback.clicked.connect(self.edit_callback)
        self.Undo.clicked.connect(self.undo_callback)
        self.Teach.clicked.connect(self.on_teach)
        self.DelHint.clicked.connect(self.del_hint)
        self.DelResponse.clicked.connect(self.del_response)
        self.SaveAll.clicked.connect(self.on_save_all)
        self.EditSavefileName.clicked.connect(self.on_edit_savefile)
        self.build()

    def on_edit_savefile(self):
        text = self.get_text('Edit savefile name', 'Press okay when done')
        self.filename = text
        self.refresh()

    def on_add_action(self):
        print("TODO")

    def on_save_all(self):
        self.classifier.save_all(self.filename)
        self.refresh()

    def on_status(self):
        pass

    def on_teach(self):
        if self.changed:
            self.classifier.classifier_learn(db_adapter(self.database))
            print("Done")

    def undo_callback(self):
        if self.changed:
            self.load_db(True)

    def get_text(self, title=None, text=None):
        # noinspection PyCallByClass
        text, ok = QtGui.QInputDialog.getText(self, title, text)
        if ok:
            return str(text)

    def add_response(self):
        payload = self.get_text("Add response", "Enter a new response")
        if payload:
            self.database.action_add_responses(str(self.current_action), payload)
            self.changed = True
            self.refresh()

    def add_hint(self):
        payload = self.get_text("Add hint", "Enter a new hint")
        if payload:
            self.database.action_add_hints(str(self.current_action), payload)
            self.changed = True
            self.refresh()

    def del_hint(self):
        try:
            x = self.HintsList.currentItem().text()
        except AttributeError:
            return
        print(x)
        action = self.current_action
        self.database.action_del_hint(action, x)
        self.refresh()

    def del_response(self):
        try:
            x = self.ResponseList.currentItem().text()
        except AttributeError:
            return
        print(x)
        action = self.current_action
        self.database.action_del_response(action, x)
        self.refresh()

    def edit_callback(self):
        payload = self.get_text("New Callback", "Enter a new callback")
        if payload:
            cont = self.showDialog("Warning", "Be careful changing callback!", "Press okay if you're sure")
            if not cont == QtGui.QMessageBox.Ok:
                return
            print(str(self.current_action), 'callback', payload)
            self.database.action_modify_section(str(self.current_action), 'callback', payload)
            self.changed = True
            self.refresh()

    def load_db(self, path=None):
        # noinspection PyArgumenList
        if not path:
            path = str(QFileDialog.getOpenFileName())
            if self.changed and path:
                cont = self.showDialog("Warning", "You have unsaved changes in this database!", "Press okay if you're sure")
                if not cont == QtGui.QMessageBox.Ok:
                    return
            if path[-4:] == '.bin':
                self.database.db_load(path[:-4])
                self.db_path = path[:-4]
                self.build()
                self.changed = False
                return
        else:
            self.database.db_load(self.database.db_path)
            self.db_path = self.database.db_path
            self.build()
            self.changed = False

    def save_db(self):
        if self.changed:
            cont = self.showDialog("Warning", "Are you sure you want to save this database!", "Press okay if you're sure")
            if not cont == QtGui.QMessageBox.Ok:
                return
            self.database.db_save(self.filename)
            self.changed = False
            self.refresh(False)

    def build(self):
        #  Action menu
        actions = QtCore.QStringList(self.database.actions)
        self.ActionList.clear()
        self.ActionList.addItems(actions)
        self.current_action = self.ActionList.currentText()
        contents = self.database.action_contents(str(self.current_action))

        #  Callback label thing
        callback = contents['callback']
        self.callback.addItem(QtCore.QString(callback))

        hints = QtCore.QStringList(contents['cmd_hints'])
        self.HintsList.addItems(hints)

        responses = QtCore.QStringList(contents['responses'])
        self.ResponseList.addItems(responses)
        self.refresh(False)

    def refresh(self, changed=True):
        if changed:
            self.changed = True
        self.callback.clear()
        self.HintsList.clear()
        self.ResponseList.clear()
        self.DbName.clear()
        self.FileName.clear()
        self.Synced.clear()

        self.current_action = str(self.ActionList.currentText())
        contents = self.database.action_contents(self.current_action)

        #  Callback label thing
        callback = contents['callback']
        self.callback.addItem(QtCore.QString(callback))

        hints = QtCore.QStringList(contents['cmd_hints'])
        self.HintsList.addItems(hints)

        responses = QtCore.QStringList(contents['responses'])
        self.ResponseList.addItems(responses)

        db_name = QtCore.QString(self.database.db_name)
        self.DbName.addItem(db_name)

        file_name = QtCore.QString(os.path.basename(self.database.db_path))
        self.FileName.addItem(file_name + ".bin")

        synced = "Synced" if self.classifier.synced else "Not synced!"
        self.Synced.addItem(synced)

    def showDialog(self, title, text, expanded_info):
        msg = QtGui.QMessageBox()
        msg.setIcon(QtGui.QMessageBox.Information)
        msg.setWindowTitle(title)
        msg.setText(text)
        msg.setInformativeText(expanded_info)
        msg.setStandardButtons(QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
        # msg.buttonClicked.connect(get_okay)
        # noinspection PyUnresolvedReferences
        retval = msg.exec_()
        return retval


def get_okay(i):
    data = {'OK': True, 'Cancel': False}
    return data[str(i.text())]


def showDialogInfo():
    msg = QtGui.QMessageBox()
    msg.setIcon(QtGui.QMessageBox.Information)
    msg.setText("This is a message box")
    msg.setInformativeText("This is additional information")
    msg.setWindowTitle("MessageBox demo")
    msg.setDetailedText("The details are as follows:")
    msg.setStandardButtons(QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
    # noinspection PyUnresolvedReferences
    retval = msg.exec_()
    return retval.text


def main():
    app = QtGui.QApplication(sys.argv)  # A new instance of QApplication
    form = ExampleApp()                 # We set the form to be our ExampleApp (design)
    form.show()                         # Show the form
    app.exec_()                         # and execute the app


if __name__ == '__main__':              # if we're running file directly and not importing it
    main()                              # run the main function
