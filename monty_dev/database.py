#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Copyright 2016 Scott Doucet

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
import os
from pprint import pprint
from ast import literal_eval
# from profilehooks import profile, timecall

PATH = os.path.dirname(os.path.abspath(__file__))


class ActionDB(object):
    default_db_path = os.path.join(PATH, "commands", "commands")

    def __init__(self, path=None):
        # self.db_path = path + ".bin" if path is not None else self.default_db_path
        if path is None:
            path = self.default_db_path
        self.db_path = path
        self.db_name = None
        self.data = {}
        self.db_load(self.db_path)

    @property
    def cmd_list(self):
        return self.data.keys()

    def db_load(self, db_file):
        """ Loads a Database file from a specified path.
            If no path is given it will load the default
            database.

        """
        with open(db_file + '.bin', 'r') as db:
            result = literal_eval(db.read())
            self.data = result['data'].copy()
            self.db_name = result['name']
            if len(self.data) == 0:
                print "Database is empty!"
            else:
                print "Database loaded"
        self.db_path = db_file

    def db_save(self, save_as):
        """ Save current database to specified path. Saves to default
            database location if no path is given.

            TODO: This may be a bad setup.. Possibly overwrite a db..

        """
        with open(save_as + '.bin', 'w') as db:
            payload = {
                'name': self.db_name,
                'data': self.data.copy()
            }
            pprint(payload, stream=db)
            print "Database saved to {}!".format(save_as)
            return True

    def db_wipe(self):
        confirm = "Are you sure? You could just make a new database."
        if not confirm:
            print "Canceled!"
            return False
        self.data = {}
        print "Database completely wiped!"
        return True

    @property
    def actions(self):
        return self.keys()

    def items(self):
        return self.data.items()

    def keys(self):
        return self.data.keys()

    def values(self):
        return [self.data[key] for key in self.data]

    def iteritems(self):
        return ((k, v) for k, v in self.data.items())

    def itervalues(self):
        return (self.data[key] for key in self.data)

    def _pprint_db(self):
        pprint(self.data.copy())
        return True

    def __contains__(self, key):
        return True if key in self.data.keys() else False

    def __repr__(self):
        return "Database: {}\n{} actions available: {}".format(self.db_name,
                                                               len(self.cmd_list),
                                                               " - ".join([i for i in self.cmd_list]))

    def __iter__(self):
        return self.data.iteritems()

    def __getitem__(self, key):
        try:
            return self.data.__getitem__(key)
        except KeyError:
            return False

    @property
    def db_training_set(self):
        keys = [i for i in self.data.keys()]
        storage = []
        for key in keys:
            action = self.data[key]
            hints = action['cmd_hints']
            callback = [action['callback'] for _ in range(len(hints))]
            result = zip(hints, callback)
            storage += result
        payload = set()
        for i in storage:
            payload.add(i)
        return payload

    def action_new(self, new_action, callback=None):
        """ Create a new action

        """
        if new_action in self.cmd_list:
            # print 'The action "{}" already exists.'.format(new_action)
            return False
        elif callback:
            template = {"callback": callback,
                        "responses": [],
                        "cmd_hints": []}
            self.data.__setitem__(new_action, template)
            # self.update_db()
            # print 'New action added \n "{}"'.format(self.Database[new_action])
            return True
        else:
            # print "You have created a new empty action. Use add_hints, add_responses " \
            #       "and modify_section to modify."
            return True

    def action_add_hints(self, action, new_hints):
        """ Add hints to an existing action
        """
        try:
            section = self.data[action]['cmd_hints']
            if isinstance(new_hints, basestring):
                section.append(new_hints)
                if len(section) < len(self.data[action]['cmd_hints']):
                    print "ERROR"
                    return
                self.data[action]['cmd_hints'] = section
                print self.data[action]['cmd_hints']
                print "Added hint!"
            elif isinstance(new_hints, list):
                result = section + new_hints
                self.data[action]['cmd_hints'] = result
                print "Added hints!"
        except KeyError:
            print "The action - {},  doesn't exist".format(action)
        pprint(self.data[action])
        return True

    def action_add_responses(self, action, new_hints):
        try:
            section = self.data[action]['responses']

            if isinstance(new_hints, basestring):
                section.append(new_hints)
                if len(section) < len(self.data[action]['responses']):
                    print "ERROR"
                    return
                self.data[action]['responses'] = section
                print "Added response!"
            elif isinstance(new_hints, list):
                result = section + new_hints
                self.data[action]['responses'] = result
                print "Added responses!"
        except KeyError:
            print "The action - {},  doesn't exist".format(action)
        pprint(self.data[action])
        return True

    def action_modify_section(self, action, section, new_values):
        try:
            data = self.data[action][section]
        except KeyError:
            print "Action: {}, Section: {},  doesn't exist".format(action, section)
            return
        if isinstance(new_values, basestring):
            try:
                data.append(new_values)
                if len(section) < len(self.data[action][section]):
                    print "ERROR"
                self.data[action][section] = data
            except AttributeError:
                data = new_values
                self.data[action][section] = data
            print "Section modified.!"
        elif isinstance(new_values, list):
            result = section + new_values
            self.data[action][section] = result
            print "Section modified.!"
        pprint(self.data[action][section])
        return True

    def action_remove_section(self, key):
        try:
            self.data.__delitem__(key)
            self.cmd_list.remove(key)
            # self.update_db()
            print 'Deleted command "{}"!'.format(key)
        except KeyError:
            print 'The command "{}", does not exist.!'.format(key)
            return False

    def action_del_hint(self, action, hint):
        try:
            section_data = self.data[action]['cmd_hints']
            index = section_data.index(hint)
            section_data.pop(index)
            # self.cmd_list.remove(key)
            self.data[action]['cmd_hints'] = section_data
            # self.update_db()
            print 'Deleted hint "{}"!'.format(hint)
        except KeyError:
            print 'The hint "{}", does not exist.!'.format(hint)
            return False

    def action_del_response(self, action, response):
        try:
            section_data = self.data[action]['responses']
            index = section_data.index(response)
            section_data.pop(index)
            # self.cmd_list.remove(key)
            self.data[action]['responses'] = section_data
            # self.update_db()
            print 'Deleted response "{}"!'.format(response)
        except KeyError:
            print 'The response "{}", does not exist.!'.format(response)
            return False

    def data(self):
        return self.data

    def action_responses(self, callback):
        action = "".join([i.capitalize() for i in callback.split('_')])
        return self.data[action]['responses']

    def action_contents(self, action):
        try:
            return self.data[action]
        except KeyError:
            return None

    def action_view_section(self, action, section):
        try:
            return self.data[action][section]
        except KeyError:
            return None


def test_db():
    test_action_db = ActionDB()
    # print(test_action_db.action_contents('GoogleSearch'))
    # print(test_action_db.cmd_list)
    print(test_action_db.action_contents('GoogleSearch'))
    print(test_action_db.action_responses('google_search'))

