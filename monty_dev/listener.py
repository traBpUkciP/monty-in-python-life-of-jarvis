#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Copyright 2016 Scott Doucet

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
from __future__ import print_function
import os
import threading
import logging
from Queue import Queue

import speech_recognition as sr


PATH = os.path.dirname(os.path.abspath(__file__))

##################
MIC_INDEX = None    # TODO: Move this to config
##################

# logging.basicConfig(level=logging.DEBUG,
#                     format='(%(threadName)-9s) %(message)s - %(asctime)s.%(msecs)03d',
#                     datefmt='%H:%M:%S',
#                     filename='errors.log',
#                     )


class Listener(object):
    is_listening = True
    is_recognizing = True
    _lt = None
    _rt = None

    def __init__(self):
        self.recognized_speech = Queue()
        self._current_audio = Queue()
        self._Recognizer = sr.Recognizer()
        Listener._lt = threading.Thread(target=self._listener)
        Listener._rt = threading.Thread(target=self._recognizer)
        Listener._lt.daemon = True                         # Demonize thread
        Listener._rt.daemon = True
        Listener._lt.start()                               # Start the execution
        Listener._rt.start()

    def _listener(self):
        logging.debug("Listener Started")
        while True:
            if self.is_listening:
                logging.debug("Listener awaiting input")
                with sr.Microphone() as source:
                    self._Recognizer.adjust_for_ambient_noise(source, duration=1)
                    audio = self._Recognizer.listen(source)
                    if self.is_listening:
                        logging.debug("Audio detected")
                        self._current_audio.put_nowait(audio)

    def _recognizer(self):
        logging.debug("Recognizer Started")
        while True:
            if self.is_recognizing:
                logging.debug("Recognizer awaiting task")
                try:
                    response = str(self._Recognizer.recognize_google(self._current_audio.get())).lower()
                    if response is not None and self.is_recognizing:
                        logging.debug("Audio recognized: {}".format(response))
                        self.recognized_speech.put(response)
                    else:
                        logging.debug("False alarm")
                except Exception as e:
                    logging.debug("Exception: {}".format(e))
                    pass

    @property
    def heard(self):
        return not self.recognized_speech.empty()

    @property
    def status(self):
        return "Listening" if self.is_listening else "Listening disabled"

    def toggle(self):
        self.is_listening = not self.is_listening


def test_listener():
    test = Listener()
    while 1:
        try:
            print("Waiting for input")
            print(test.recognized_speech.get())
        except KeyboardInterrupt:
            break
    quit()
