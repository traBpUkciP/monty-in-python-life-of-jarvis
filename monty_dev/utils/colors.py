# -*- coding: utf-8 -*-
"""
    Copyright 2016 Scott Doucet

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from __future__ import print_function
from colorama import Fore, Back, Style, init
import time
from random import choice, random
init(autoreset=True)

colors = {
    "RED" : Fore.RED,
    "BLUE" : Fore.BLUE,
    "YELLOW" : Fore.YELLOW,
    "GREEN" : Fore.GREEN,
    "CYAN" : Fore.CYAN
}

def CustomForm(header, data, color, overwrite=False):

    white_space = " " * (15 - len(header))
    # print(header)
    # print(data)
    # print(color)

    if overwrite:
        print(colors[color.upper()] + "\r[{}{}]: {}".format(header, white_space, data), end="")
    else:
        print(colors[color.upper()] + "[{}{}]: {}".format(header, white_space, data))

def ColorError(data, overwrite=False):
    if overwrite:
        print(Fore.RED + "\r[ERROR          ]: {}".format(data), end="")
    else:
        print(Fore.RED + "[ERROR          ]: {}".format(data))

def ColorWarning(data, overwrite=False):
    if overwrite:
        print(Fore.YELLOW + "\r[WARNING        ]: {}".format(data), end="")
    else:
        print(Fore.YELLOW + "[WARNING        ]: {}".format(data))

def ColorInfo(data, overwrite=False):
    if overwrite:
        print(Fore.GREEN + "\r[INFO           ]: {}".format(data), end="")
    else:
        print(Fore.GREEN + "[INFO           ]: {}".format(data))

def ColorBlue(data, overwrite=False):
    if overwrite:
        print(Fore.CYAN + "\r{}".format(data + Style.RESET_ALL), end="")
    else:
        print(Fore.CYAN + "{}".format(data + Style.RESET_ALL))

def ColorRed(data, overwrite=False):
    if overwrite:
        print(Fore.RED + "\r{}".format(data + Style.RESET_ALL), end="")
    else:
        print(Fore.RED + "{}".format(data + Style.RESET_ALL))

def ColorYellow(data, overwrite=False):
    if overwrite:
        print(Fore.YELLOW + "\r{}".format(data + Style.RESET_ALL), end="")
    else:
        print(Fore.YELLOW + "{}".format(data + Style.RESET_ALL))

def ColorGreen(data, overwrite=False):
    if overwrite:
        print(Fore.GREEN + "\r{}".format(data + Style.RESET_ALL), end="")
    else:
        print(Fore.GREEN + "{}".format(data + Style.RESET_ALL))

def ColorCritical(data, overwrite=False):
    if overwrite:
        print(Back.RED + "\r[CRITICAL       ]: {}".format(data), end="")
    else:
        print(Back.RED + "[CRITICAL       ]: {}".format(data))


if __name__ == '__main__':

    print("%s should be blue" % ColorBlue('This'))
    print(ColorBlue('This should all be blue'))
    print(ColorYellow('This should all be yellow'))
    print(ColorGreen('This should all be green'))
    print(ColorRed('This should all be red'))
    ColorCritical("This is a critical error")
    ColorError("This is an error")
    ColorWarning("This is a warning")
    ColorInfo("This is an info line")
    CustomForm("Custom Header", "Custom headers can fit 15 characters", "red")
    CustomForm("Custom Header 2", "They can also be different colors", "yellow")
    CustomForm("Custom Header 3", "You can also specify carriage return ", "green")
    CustomForm("Slayer Custom", "although that's not demonstrated.", "blue")

    for i in range(121):
        color = choice(colors.keys())
        CustomForm("{}".format(color), ("#" * i), color, overwrite=True)
        time.sleep(.15)
