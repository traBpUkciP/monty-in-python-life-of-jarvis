#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Copyright 2016 Scott Doucet

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
from __future__ import print_function
import io
import os
import difflib
import time
from pprint import pprint
import inspect
import functools
import json
import xmltodict

THIS_DIR = os.path.dirname(os.path.realpath(__file__))

test_data = {
    "mydocument": {
        "@has": "an attribute", 
        "and": {
            "many": [
                "elements", 
                "more elements"
            ]
        }, 
        "plus": {
            "@a": "complex", 
            "#text": "element as well"
        }
    }
} 

target = '''<mydocument has="an attribute">
  <and>
    <many>elements</many>
    <many>more elements</many>
  </and>
  <plus a="complex">
    element as well
  </plus>
</mydocument>
'''

def load_from_xml(path):
    """
    """
    with open(path, 'r') as file_:

        # return json.dumps(xmltodict.parse(file_, encoding='utf-8'))
        return xmltodict.parse(file_, encoding='utf-8')
    return False


def save_to_xml(data, path):
    """
    """
    with open(path, 'w') as file_:
        return file_.write(xmltodict.unparse(data, pretty=True, encoding='utf-8'))
    return False


def load_from_json(path):
    """
        Load a json formatted file from `path`
    """
    with open(path, 'r') as file_:
        return json.load(file_)
    return False


def save_to_json(data, path):
    """
        Save `data` to `path` as json
    """
    with open(path, 'w') as file_:
        return json.dump(data, file_)
    return False


def catch_exceptions(job_func):
    # noinspection PyBroadException
    @functools.wraps(job_func)
    def wrapper(*args, **kwargs):
        try:
            job_func(*args, **kwargs)
        except:
            import traceback
            print(traceback.format_exc())
    return wrapper


@catch_exceptions
def bad_task():
    return 1 / 0


def get_text_diff(text1, text2):
    delta = ''
    for i, s in enumerate(difflib.ndiff(text1, text2)):
        if s[0] == ' ' : continue
        elif s[0] == '+':
            delta += s[-1]
    return delta.strip()


def print_attributes(cls):
    attributes = inspect.getmembers(cls, lambda a:not(inspect.isroutine(a)))
    pprint([a[0] for a in attributes if '__' not in a[0]])


def return_attributes(cls):
    attributes = inspect.getmembers(cls, lambda a:not(inspect.isroutine(a)))
    return [a[0] for a in attributes if '__' not in a[0]]


def print_methods(cls):
    pprint([i for i in dir(cls) if '__' not in i])


def return_methods(cls):
    return [i for i in dir(cls) if '__' not in i]


def func_timer(func):
    t1 = time.clock()
    def func_wrapper(*args, **kwargs):
        func(*args, **kwargs)
    t2 = time.clock()
    print('The code took {:.2f}ms'.format((t2 - t1) * 1000.))


def Terminal(cls):
    import getpass
    self = cls
    while True:
        sudo = 0
        quits = ["exit", "quit", "q"]
        test_question = raw_input("traBpUkciP@slayerNet:~$ ") or None
        if test_question is None:
            print("D'oh!")
            continue
        elif test_question in quits:
            exit()
        elif test_question == 'help':
            print_methods(self)
        elif test_question == "sudo" or "hack":
            sudo = 1
            tries = 0
            while True:
                if tries < 3:
                    password = getpass.getpass("[sudo] password for traBpUkciP:")
                    if password != "'":
                        tries += 1
                        print("Sorry. Try again.")
                    else:
                        break
                else:
                    print("sudo: 3 incorrect password attempts")

            while sudo:
                cmd = raw_input( "root@slayerNet:~$ " ) or None
                if cmd is None:
                    print("D'oh!")
                    continue
                elif cmd in quits:
                    break
                elif cmd == "help":
                    print_methods(self)
                try:
                    exec(cmd)
                except Exception as e:
                    print(e)

def get_diff(a, b):
    return set.intersection(set(a), set(b))

if __name__ == '__main__':
    # from grun import grun
    # grun((print_methods, return_methods, Terminal))
    # text1 = "search youtube for ninja turtles"
    # text2 = 'youtube search'
    # print(get_text_diff(text2, text1))
    #
    # text2 = 'search youtube'
    # print(get_text_diff(text1, text2))
    #
    # text2 = 'youtube search for'
    # print(get_text_diff(text1, text2))
    #
    # text2 = 'search on youtube for'
    # print(get_text_diff(text1, text2))

    # print(get_diff(text1, text2))
    a = load_from_xml(os.path.join(THIS_DIR, 'testData.xml'))
    print(a)
    # y = save_to_xml(test_data, 'testData.xml')
    # print(y)