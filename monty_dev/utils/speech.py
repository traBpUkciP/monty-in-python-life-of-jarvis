#!/usr/bin/env python
# encoding: utf-8
import os
import platform
import pyttsx
import threading
from pprint import pprint


def old_code():
    if platform.system() == "Windows":
        import win32com.client
        speaker = win32com.client.Dispatch("SAPI.SpVoice")


class SpeechEngine(object):

    def __init__(self):
        self._engine = pyttsx.init()
        self._voices = self._engine.getProperty('voices')

    def block(self):
        self._engine.runAndWait()

    def start(self):
        self._engine.startLoop(False)

    def update(self):
        self._engine.iterate()

    def speak(self, text, name=None):
        self._engine.say(text, name)

    def set_speaker(self, name):
        for voice in self._engine.getProperty('voices'):
            # print voice.name
            if name in voice.name:
                print "Booger"
                print voice.id
                self._engine.setProperty('voice', voice.id)

    def adjust_rate(self, adjustment):
        rate = self._engine.getProperty('rate')
        self._engine.setProperty('rate', rate + adjustment)

    def set_rate(self, rate):
        self._engine.setProperty('rate', rate)

    def set_volume(self, volume):
        self._engine.setProperty('volume', volume)

    def get_property(self, name):
        self._engine.getProperty(name)

    def destroy(self):
        self._engine.endLoop()

    @property
    def speaker_list(self):
        return [[index, voice.name] for index, voice in enumerate(self._voices)]

    def current_speaker(self):
        return self._engine.getProperty('voice')

    @property
    def rate(self):
        return self._engine.getProperty('rate')

    @property
    def volume(self):
        return self._engine.getProperty('volume')


if __name__ == '__main__':
    test = SpeechEngine()
    # test.start()
    print "First: ", test.current_speaker()
    test.speak("Boogers")
    test.set_speaker('David')
    print "Second: ", test.current_speaker()
    print test.speaker_list
    test.speak("Boogers")
    # test.destroy()
    test.block()
"""
def speak(speech, voice='sapi'):
    def _play_mp3(file):
        pygame.mixer.music.load(file)
        pygame.mixer.music.play()
    if voice == 'sapi':
        Speak = speaker.Speak
        Speak(speech)
    elif voice == 'google':
        file_path = "{}.mp3".format(speech)
        if os.path.isfile(file_path):
            _play_mp3(file_path)
        else:
            tts = gTTS(text=speech, lang='en')
            tts.save(file_path)
            _play_mp3(file_path)
            # while pygame.mixer.music.get_busy() == True:
            #     continue

def _get_google_voice(speech):
    def _no_block(speech):
        tts = gTTS(text=speech, lang='en')
        tts.save(file_path)
        _play_mp3(file_path)
        # while pygame.mixer.music.get_busy() == True:
        #     continue
    demon = threading.Thread(target=_no_block, args=[speech])
    demon.daemon = True
    demon.start()

def _play_mp3(file):
    pygame.mixer.music.load(file)
    pygame.mixer.music.play()

def get_current_ai_info():
    pass



if __name__ == '__main__':
    pprint(list_voices())
    raw_input()
"""