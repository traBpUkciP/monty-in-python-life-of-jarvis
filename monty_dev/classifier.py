#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Copyright 2016 Scott Doucet

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
import os
import logging

import dill as pickle
from textblob.classifiers import NaiveBayesClassifier

from .utils.tools import print_methods
from .database import ActionDB


class Classifier(object):
    """
        A custom intent engine based on the NLTK NaiveBayesClassifier.

    """
    def __init__(self, path, config=None):
        """
            Initializes a classifier instance from `path` if it exists.
            If not creates a new classifier instance residing in `path`
        """
        self.config = config
        self.save_file = path
        self._internal = self.classifier_load(self.save_file)
        if self._internal is None:
            logging.debug("New Classifier: {}.".format(path))

    def classifier_load(self, path=None):
        """
            Loads a saved classifier instance pickle from the `save_file` instance 
            attribute or `path` if provided. Returns None if file doesn't exist or
            if pickle file is corrupted.

            TODO: (to self) Fix this appending the path name bullshit please. Merp.
        """
        # payload = None
        try:
            with open(self.save_file if not path else path + '.data', 'rb') as f:
                payload = pickle.load(f)
                logging.debug("Classifier loaded.")
                return payload
        except IOError:
            logging.debug("Error loading {}".format(self.save_file))
            return None
        except KeyError:
            logging.debug("Datafile - {} - is corrupted".format(path))
            return None

    def classifier_save(self, path=None):
        """
            Saves internal NLTK NaiveBayesClassifier instance to `path`

            TODO: (to self) Fix this appending the path name bullshit please. Merp.
        """
        try:
            with open(self.save_file + '.data' if not path else path + '.data', 'wb') as f:
                pickle.dump(self._internal, f)
                logging.debug("Classifier saved.")
        except IOError:
            logging.debug("Error loading {}".format(self.save_file))
            return None

    def classifier_learn(self, training_data):
        """ 
            `training_data = [(callback: Function, cmd_hints: List),]`

            Feed `training_data` to the intent engine
        
        """
        if isinstance(training_data, tuple):
            training_data = [training_data]
        if self._internal is None:
            self._internal = NaiveBayesClassifier(training_data)
        else:
            self._internal.update(training_data)

    def classifier_classify(self, cmd):
        """
            Get's intent for `cmd` from the NaiveBayesClassifier
            returns: tuple(
                        intent: callback
                        confidence: integer
                        cmd: the incoming `cmd`
                     ) 
        """
        result = self._internal.prob_classify(cmd)
        intent = result.max()
        confidence = round(result.prob(result.max()), 2)
        return intent, confidence, cmd

    @property
    def training_set(self):
        """
            Returns a list containing the training data the current
            NaiveBayesClassifier is trained on.
        """
        data = self._internal.train_set[:]
        payload = set()
        for i in data:
            payload.add(i)
        return payload

    @property
    def actions(self):
        """
            Returns a list of the current classifier actions
        """
        return self._internal.classifier.labels()

    @staticmethod
    def classifier_delete(filename):
        """
            More like "delete `filename`".. Careful
        """
        try:
            os.remove(filename)
        except OSError as e:
            return "Failed with error: {}".format(e)


class ClassifierDB(Classifier):
    def __init__(self, *args, **kwargs):
        super(ClassifierDB, self).__init__(*args, **kwargs)
        self.database = ActionDB(*args, **kwargs)

    @property
    def synced(self):
        data = self.database.db_training_set ^ self.training_set
        if len(data) == 0:
            return True
        return False

    def save_all(self, path):
        self.database.db_save(path)
        self.classifier_save(path)


def action_adapter(action):
    callback = action['callback']
    hints = action['cmd_hints']
    result = [(i, callback) for i in hints]
    return result


def db_adapter(db):
    result = []
    for k, v in db.iteritems():
        callback = v['callback']
        hints = v['cmd_hints']
        result += [(hint, callback) for hint in hints]
    return result


def main(cls):
    import getpass
    self = cls
    while True:
        # sudo = 0
        quits = ["exit", "quit", "q"]
        test_question = raw_input("traBpUkciP@slayerNet:~$ ") or None
        if test_question is None:
            print "D'oh!"
            continue
        elif test_question in quits:
            exit()
        elif test_question == "save":
            self.save_classifier()
        elif test_question == "load":
            self.load_classifier()
        elif test_question == "new":
            name = raw_input("name $ ")
            callback = raw_input("callback $ ")
            responses = raw_input("responses $ ")
            cmd_hints = raw_input("cmd_hints $ ")
            self.new(name, callback, responses, cmd_hints)
        elif test_question == "sudo" or "hack":
            sudo = 1
            tries = 0
            while True:
                if tries < 3:
                    password = getpass.getpass("[sudo] password for traBpUkciP:")
                    if password != "'":
                        tries += 1
                        print "Sorry. Try again."
                    else:
                        break
                else:
                    print "sudo: 3 incorrect password attempts"
                    main(cls)

            while sudo:
                cmd = raw_input("root@slayerNet:~$ ") or None
                if cmd is None:
                    print "D'oh!"
                    continue
                elif cmd in quits:
                    break
                elif cmd == "help":
                    print_methods(self)
                try:
                    exec cmd
                except Exception as e:
                    print e
        else:
            result = brain.classifier_classify(test_question)
            print """
            Command: {} \n
            Intent: {} \n
            Confidence: {} \n
            """.format(test_question, result[0], result[1])


def test():
    while True:
        test_question = raw_input("$ ") or None
        if test_question is None:
            break
        result = brain.classifier_classify(test_question)
        print """
        Command: {} \n
        Intent: {} \n
        Confidence: {} \n
        """.format(test_question, result[0], result[1])

def test_classifier():
    PATH = os.path.dirname(os.path.abspath(__file__))
    default_classifier_path = os.path.join(PATH, 'classifier')
    brain = ClassifierDB(default_classifier_path)
    print(brain.training_set)
    print(brain.database.db_training_set)
    print(brain.synced)
    # print(brain.classifier_classify("play a song for me please"))
    #
    # classifier_db = ClassifierDB(default_classifier_path)
    # print(classifier_db.classifier_classify("play a song for me please"))
    # classifier_db.save_all('classifier_adv')
    #
    # classifier_db = ClassifierDB(PATH + '\\classifier_adv')
    # print(classifier_db.classifier_classify("play a song for me please"))

