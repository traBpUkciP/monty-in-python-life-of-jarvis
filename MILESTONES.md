Milestones
==========

The purpose of this file is to list and keep 
track of all the needed components to satisfy 
a working Linux version of links.

The Big list
============
Each item of the list will have a priority rating next to the name.
The rating is based out on 3 choices (urgent/necessary/minor).

**GUI / Theme** *necessary*
If it looks like a duck and quacks like a duck..

 - Intro video
 The opening portion of the app should be as similar as 
 possible to hold to the principle of minimal astonishment.

 - Graphical skinning. 
 This is an interesting one. Not reproducing the GUI exactly
 may be a good thing in the sense that most Linux users don't 
 see their desktop as often as windows users so a scaled down
 yet similarly themed desktop (or taskbar widget) may be 
 sufficient.

 - Sounds must be similar. 
 Voice packs are not included in this. Only the native GUI 
 sounds.

**Wordlists** *necessary*
Wordlist are a big part of links and must be implemented
in a cross compatible way. What works on windows should work
on Linux.

**Commands** *urgent*
Commands being the focal point of the app, these must be 
implemented as a top priority. From the importing and exporting
of backups this must be done right.

 - Shell
 Shell commands should be implemented to support the different
 linux shells. ie: bash, zsh, etc..

 - Web
 These commands should be the same as on windows.

 - Social
 These commands can also be the same as on windows.

 - default
 Cross compatible default commands should be considered.

**Keyboard support** *minor*
Should be the same as the windows version.

**UserSettings** *minor*
This should be as cross compatible as possible. XML is rather
standard and shouldn't pose a prblem.

**Web Server** *minor*
Must be able to speak with windows version and otherwise be as 
cross compatible as possible.

**Plugins** *minor*
Plugins cannot be shared and must be maintained in seperate
locations. This cannot be helped unless some sort of cross
compilation step can be implemented (See Iron Python)

**This list is by no means exhaustive and all additions/modifications will be considered.**

Author
------
Scott Doucet