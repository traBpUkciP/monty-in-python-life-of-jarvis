Monty In Python
===============

Installation
------------
Clone repository and install (Recommended dev install shown)    

```
#!shell

    $ git clone https://traBpUkciP@bitbucket.org/traBpUkciP/monty-in-python-life-of-jarvis.git
    $ cd monty-in-python-life-of-jarvis
    $ pip install -e .
    $ python manage.py setup
```


Running
-------
The manage.py file contains all the entry points for running and testing. Including access to the GUI backend for the database.

Standard run in debug mode (ie: debug=True)

```
#!shell

    $ python manage.py debug
```

    
Run in live mode (ie: debug=False)    

```
#!shell

    $ python manage.py deploy
```


Open GUI database backend
```
#!shell

    $ python manage.py gui
```


Examples
--------
TODO

License
-------
Apache 2

Credits
-------
TODO

Links
-----

Author
------
Scott Doucet